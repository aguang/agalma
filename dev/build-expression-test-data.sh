#!/bin/sh

# fastq-dump from sratoolkit version 2.2.0
wget http://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByRun/sra/SRR/SRR089/SRR089297/SRR089297.sra
fastq-dump SRR089297.sra

wget http://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByRun/sra/SRR/SRR081/SRR081276/SRR081276.sra
fastq-dump SRR081276.sra

# path to the Nanomia bijuga assembly from the Agalma paper test analysis
ASSEMBLY=assembly_trinity_31728366.annotated.fa

head -10000 $ASSEMBLY >Nanomia_bijuga.fa

# bowtie2 version 2.1.0
bowtie2-build Nanomia_bijuga.fa Nanomia_bijuga
bowtie2 --very-fast-local -p 8 -x Nanomia_bijuga -U SRR089297.fastq --al SRX036876.fq >/dev/null
bowtie2 --very-fast-local -p 8 -x Nanomia_bijuga -U SRR081276.fastq --al SRX033366.fq >/dev/null

