#!/usr/bin/env python

import crcmod
import re
import sys

_crc64 = crcmod.mkCrcFun(
		0b10000000000000000000000000000000000000000000000000000000000011011,
		initCrc=0x0000)
crc64 = lambda x : '%016x' % _crc64(x)

for line in open(sys.argv[1]):
	if line.startswith('ID'):
		ID = line[5:].partition(' ')[0]
		AC = None
		DE = None
		OS = None
		OG = None
		SQ = False
		seq = []
	elif line.startswith('AC'):
		if not AC:
			AC = line[5:].partition(';')[0]
	elif line.startswith('DE'):
		if not DE:
			DE = line.partition('=')[2].partition(';')[0]
	elif line.startswith('OS'):
		if not OS:
			OS = line[5:].partition('.')[0]
		else:
			OS = OS.rstrip() + line[5:].partition('.')[0]
	elif line.startswith('OG'):
		OG = re.split('[ .;]', line[5:], 1)[0]
	elif line.startswith('SQ'):
		SQ = line[5:].split()[5].lower()
	elif line.startswith('//'):
		header = '>swissprot|%s|%s %s OS=%s' % (AC, ID, DE, OS)
		if OG: header += ' OG=' + OG
		assert '\n' not in header, header
		print header
		seq = ''.join(seq)
		assert crc64(seq) == SQ, ' '.join((ID, SQ, crc64(seq)))
		assert '-' not in seq, ID
		print seq
	elif SQ:
		seq.append(line.rstrip().replace(' ',''))

# vim: noexpandtab ts=4 sw=4
